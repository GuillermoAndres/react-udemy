import React from "react";
import PropTypes from 'prop-types';

// import React, { Fragment } from "react";

// Los componentes pueden esta hecho basados en:
// Clases
// Funciones 

// Functional componenets or SFC
const PrimeraApp = ({saludo, subtitulo}) => {
    return (
        <>            
            <h1>{saludo}</h1>
            <p>Mi first application con {subtitulo} </p>
        </>
    );
    
}
// Especificar el tipo de las propiedades del compponente
PrimeraApp.propTypes = {
    saludo: PropTypes.string.isRequired    
}
// Definiendo valores por defecto mediante defaultProps
PrimeraApp.defaultProps = {
    subtitulo: 'Soy un valor por defecto'
}

export default PrimeraApp;