import React from "react";
import PropTypes from 'prop-types';

// import React, { Fragment } from "react";

// Los componentes pueden esta hecho basados en:
// Clases
// Funciones 

// Functional componenets or SFC
const PrimeraApp = ({saludo}) => {
    // const PrimeraApp = ({saludo="Debe agregar un valor"}) => {
    
    // Para englobar severtals tag, we need a compenet
    // called hight order fragment
    // return (
    //     <Fragment>
    //         <h1>Hola mundo</h1>
    //         <p>Mi first application </p>
    //     </Fragment>
    // );
    
    // Otra forma de hacerlo sin fragment
    // return (
    //     <>
    //         <h1>Hola mundo</h1>
    //         <p>Mi first application </p>
    //     </>
    // );

    // let name = 'Guillermo';
    // name = [1,'hola',false];
    // name = {
    //     nombre: 'Gerardo',
    //     edad: 22,
    // };
    // name = 'Guillermo'
    // console.log(props);
    // return (
    //     <>
    //         {/* <h1>Hola {JSON.stringify(name)} </h1> */}
    //         {/* <pre>Hola {JSON.stringify(name, null, 3)} </pre> */}
    //         <h1>Hola {name}</h1>
    //         <p>Mi first application </p>
    //     </>
    // );

    // return (
    //     <>
    //         {/* <h1>Hola {JSON.stringify(name)} </h1> */}
    //         {/* <pre>Hola {JSON.stringify(name, null, 3)} </pre> */}
    //         <h1>{props.saludo}</h1>
    //         <p>Mi first application </p>
    //     </>
    // );

    // Forzar que mande un valor para saludo
    // if (!saludo) {
    //     throw new Error('El saludo es necesario');
    // }

    // Otra forma de hacerlo es importando PropTypes
    
    return (
        <>
            {/* <h1>Hola {JSON.stringify(name)} </h1> */}
            {/* <pre>Hola {JSON.stringify(name, null, 3)} </pre> */}
            <h1>{saludo}</h1>
            <p>Mi first application a </p>
        </>
    );
    
}
// Especificar el tipo de las propiedades del compponente
PrimeraApp.propTypes = {
    saludo: PropTypes.string.isRequired
    //otra: Prop//ReactDOM.render(<PrimeraApp saludo={123}/>, divRoot);Types.number.isRequired
}

export default PrimeraApp;