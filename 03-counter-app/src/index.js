import React from "react";  // React ahora esta en nuestro scope    
import ReactDOM from "react-dom";
import PrimeraApp from "./PrimeraApp";
import './index.css';

// Seleccionamos el elementoen el cual ingresaremos las componentes
const divRoot = document.querySelector('#root');
// console.log(divRoot);


// 1) Primera manera de renderizar componentes
// Para empezar utilizar etiqueta html en un doumento js, se necesita importar
// react para que no haya ningun error
// const saludo = <h1>Hola mundo</h1>;
// console.log(saludo);
// ReactDOM.render(saludo, divRoot);




// 2) Forma de renderizar html 
// Notacion de React para utilizar una componente <Component />
// ReactDOM.render(<PrimeraApp />, divRoot);

// Mandar un valor de saludo numero {int}
// ReactDOM.render(<PrimeraApp saludo={123}/>, divRoot);
// Mandar un string
ReactDOM.render(<PrimeraApp saludo="Guillermo"/>, divRoot);


