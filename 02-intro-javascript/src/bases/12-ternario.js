// Operador condicional ternario
const activo = true;

let mensaje = "";

if (activo) {
    mensaje = "Activo"    
} else {
    mensaje = "Inactivo"
}

// Usando el operador ternario
mensaje = activo ? "Activo": "Inactivo";

// Sabemos que javascript siempre regresa el valor de la cual es verdadero segun
// el operador logico
// console.log(!!"activo");  // Conociendo el valor truthy de las cadenas
mensaje = activo && "activo";
mensaje = !activo && "activo";
mensaje = (activo === true) && "Activo"

console.log(mensaje);

