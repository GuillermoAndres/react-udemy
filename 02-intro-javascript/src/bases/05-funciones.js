// Functions

// Function con un nombre
function saludar(nombre){
    return `Hola ${nombre}`;
};

console.log(saludar('Goku'));

// Ver la referencia a la funcion
console.log(saludar);

// Podria surgir un comportamiento inesperado dado que podemos que es tipado dinamico
saludar = 99
console.log(saludar);

// Funcion de primera clase
// Es recomendatable crear funciones const de este manera no sera posible cambiar las 
// referencias 
const saludo = function(nombre) { // Esto tambien es una funcion anonima o sin nombre
    return `Hola ${nombre}`
}

console.log(saludo('Vegeta'));

// Esto genera un erro y evitamos que cambie la referencia a nuestra variable.
//saludo = 30 


// Funciones anonimas 
// Funciones lambda en Python
// Funciones flecha en Javascript
const greeting = (nombre) => {
    return `Hola ${nombre}`    
}

console.log(greeting);
console.log(greeting('Gohan'))

// Si tenemos que nuestra funcion regresa solo un return podemos simplificarlo con
const saludar3 = (nombre) => `Hola ${nombre}`
console.log(saludar3);
console.log(saludar3('Picoro'))


// Funciones que regresar objetos
let getUser = () => {
    return {
        uid: 'ABC32',
        username: 'memo123',
    }
};

console.log(getUser());


// Simplificando el codigo de arriba para indicarle que no hay confuncion con las llaves
// lo encerramos entre parentesis
getUser = () => ({
    uid: 'ABC32',
    username: 'memo123',
});

//console.log(getUser());
const user = getUser()
console.log(user);


// Ejercicio
function getUserActivo(nombre) {
    return {
        uid: '123',
        username: nombre,
    }
};

const usuarioActivo = getUserActivo('Gerardo');
console.log(usuarioActivo);

// 1. Convertirlo en una funcion flecha
// 2. Retornar un objeto implícito

const getUserActivo2 = (nombre) => ({uid: '123', username: nombre});
const usuarioActivo2 = getUserActivo2('Andrés');
console.log(usuarioActivo2);









