
// Async-Await

// Forma de obtener una imagen a traves de una promesa
const getImagenPromesa = () =>
    new Promise(resolve => resolve("https://sdfsgds.com"));

getImagenPromesa().then(console.log);

// La otra forma de hacerlo seria utilizando keword Async
const getImagen = async () => {
    return "htpps://asdfasg.com"
}
// Obtenemos una promoesa como valor devuelto.
//console.log(getImagen());
getImagen().then(console.log)


// wait: Nos ayuda a trabajar con nuestro codigo para que fuera sincrono
const getImagen2 = async () => {

    // Menejo de errores
    try {
        // Fetch API
        const apiKey = "V9L2bNnzKfMaZsCIdMDeZDFxvHVC7L9P"
        // Nos devolvera una promesa 
        const resp = await fetch(`https://api.giphy.com/v1/gifs/random?api_key=${apiKey}`);
        // Descomponemos el objeto data
        const { data } = await resp.json();
        //console.log(data); 
        const { url } = data.images.original;
        //console.log(url);

        const img = document.createElement("img");
        img.src = url;
        document.body.append(img);
    } catch (error) {
        // manejo de error
        console.error(error)
    }
}

getImagen2();


