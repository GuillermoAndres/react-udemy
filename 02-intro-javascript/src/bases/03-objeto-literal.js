// Objects literals
// De forma analoga a lo que son diccionarios en Python
let persona = {

};
console.log(persona);

// key:value
persona = {
    nombre: 'Tony',
    apellidi: 'Stack',
    edad: 45,  // Buena practica js, coma colgante  
    // Tambien es posibles agregar otros objetos
    // direccion: {
    //     ciudad: 'New Yorl',
    //     zip: 12342,
    //     lat: 14.234,
    //     lon: 98.23
    // }
};
console.log(persona);

// Crendo un objeto dentro de que tiene como atributo otro objeto
console.log({persona:persona});

// ECMAscrip6 cuando el atributo tiene el mismo nombre del objeto se puede omitir
console.log({persona});

// Otra forma de visualizarlo
console.table(persona)

// Clonando objetos

// Copiando la referencia 
let persona2 = persona 
//persona2.nombre = 'Juan'
console.log(`Persona1: \n`);
console.log(persona);
console.log(`Persona2:\n`);
console.log(persona2);
console.log('----------------');
// Clonando un objeto
// Spreed operator
// Es de forma analogo a unpacking de Python *lista, example foo(*lista)
// Example de javascrit: foo(...array)

// Shallow-cloning
// Este operador tambien sirve para copiar objetos literales
persona2 = {...persona} 
persona2.nombre = 'Juan'
console.log(`Persona1: \n`);
console.log(persona);
console.log(`Persona2:\n`);
console.log(persona2);
