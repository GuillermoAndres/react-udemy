// Fetch API
const apiKey="V9L2bNnzKfMaZsCIdMDeZDFxvHVC7L9P"

// Nos devolvera una promesa 
const peticion = fetch(`https://api.giphy.com/v1/gifs/random?api_key=${apiKey}`);

// Veremos que respuesta nos da si se ejecutar correctamente la peticion
// peticion.then((resp) => {
//     // Vemos la respuesta de toda la peticion
//     // console.log(resp);

//     // Para obtener el contenido de body: Readeable Stream
//     resp.json().then((data) => {
//         console.log(data);
//     })
// })
// .catch(console.warn);


// Otra forma de lograrlo es usar promesas encadenadas
peticion.then(resp => resp.json())
.then(({data}) => {

    const {url} = data.images.original;
    //console.log(url);
    
    const img = document.createElement("img");
    img.src = url;
    document.body.append(img);
})
.catch(console.warn)


