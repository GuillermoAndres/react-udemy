// Desestructuracion de arreglos

const personajes = ['Goku', 'Vegeta', 'Trunks']

// Accedemos al elementos
console.log(personajes[0]);
console.log(personajes[1]);
console.log(personajes[2]);
console.log('----------------');

// Aceder al primer elemento
const [p1] = personajes;
console.log(p1);

// Aceder al segundo elemento
const [, p2] = personajes
console.log(p2)

// Aceder al segundo elemento
const [, , p3] = personajes
console.log(p3)

// Destructurar un arreglo 
const retornaArreglo = () => {
    return ['ABC', 123];
};
const [letras, numeros] = retornaArreglo();
console.log(letras, numeros);

// Aplicacion
const usarState = (valor) => {
    return [valor, ()=> {console.log('Hola mundo')}];
}

const arr = usarState('Goku');
console.log(arr);

// llamando la funcion
arr[1]();

// Desestructurando variables y funciones
const [nombre, setNombre] = usarState('Gohan')
console.log(nombre, setNombre);
setNombre();





