
//console.log('Hola mundo')


// Variables y constantes
// var: esta obsoleto
const nombre = 'Guillermo'
const apellido = 'Andres'

// Valores que van cambiar a lo largo del script
let valorDado = 5;
valorDado = 4;

console.log(nombre, apellido, valorDado)


// Variables de scope con let 
// Solo va existir en este ambito {} scope
if (true) {
    const nombre = 'Jim'
    let valorDado = 99
    console.log(nombre, valorDado)
}
console.log(valorDado)