
// Template string
// Sirve para poner valor de variables u operaciones dentro de los string
const nombre = 'Guillermo';
const apellido = 'Andres';

// Concatenacion normal
let nombreCompleto = nombre + ' ' + apellido;

console.log(nombreCompleto)

// Template string
nombreCompleto = `${nombre} ${apellido}`;

console.log(nombreCompleto)

function getSaludo(nombre) {
    return 'Hola '+ nombre;
}

// Todo valor que no se inicialize va tener por defector el valor undefining
console.log(`Este es una texto: ${getSaludo()}`);

console.log(`Este es una texto: ${getSaludo('Gerardo')}`);

