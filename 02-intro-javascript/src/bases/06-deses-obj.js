// Desestructuracion
// Asignación Desestructurante
const persona = {
    nombre: 'Tony',
    edad: 45,
    clave: 'Iroman'
};

console.log(persona.nombre);
console.log(persona.edad);
console.log(persona.clave);
console.log('-------------');


// Extraer los atributos de un objeto (no importa el orden en que se llamen)
const {nombre, edad, clave,} = persona;
console.log(nombre);
console.log(edad);
console.log(clave);

// Renombrar el nombre de la variable por si ya existe en el entorno de trabajo
const {nombre:nombre2 } = persona;
console.log(nombre2);
console.log('-------------');

// Otra aplicacion de la desescturacion es a traves de los argumentos de una funcion
const retornaPersona = (usuario) => {    
    //console.log(usuario);
    const {clave, nombre, edad} = persona;
    console.log(nombre, edad,clave);
};
retornaPersona(persona)
console.log('-------------');

// Si solo nos interasa el nombre del objeto como argumento seria
// rango es valor por defecto, si no lo tiene el objeto sera undefined
const retornaPersona2 = ({nombre, edad, rango = 'Capitan'}) => {
    console.log(nombre, edad, rango);
};
retornaPersona2(persona)
console.log('-------------');


// Funcion que regresa un objeto
const usarContext = ({clave, nombre, edad, rango = 'Capitan'}) => {
    //console.log(nombre, edad, rango);
    return {
        nombreClave: clave, 
        anios: edad, 
    };
};
const avenger = usarContext(persona);
console.log(avenger);

// Ahora de este objeto que devuelve obtener el nombreClave y anios
const {nombreClave, anios} = usarContext(persona)
console.log(nombreClave, anios);

// Esto de forma analoga Unpacking en tuplas en Python
// persona = ('Iroman', 22)
// nombre, edad = persona


// Extraer objetos anidados
const usarContext2 = ({clave, nombre, edad, rango = 'Capitan'}) => {
    //console.log(nombre, edad, rango);
    return {
        nombre: clave, 
        anios: edad, 
        direccion: {
            lat: 22.3243,
            lon: 42.121,
        },
    };
};
console.log(usarContext2(persona));
// Ya existe nombres repetidos utilizamos otros
let {nombre:nombre3, anios:anios2, direccion} = usarContext2(persona);
console.log(nombre3, anios2, direccion );
// const {lat, lon} = direccion

// Otra alternativa es usando la misma notacion para nombres repetidos extraemos lat y log
const {nombre:nombre4, anios:anios3, direccion:{lat, lon}} = usarContext2(persona);
console.log(nombre4, anios3, lat, lon);