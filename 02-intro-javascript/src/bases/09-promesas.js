import {getHeroueById} from "./bases/08-imp-exp"
import { heroes } from "./data/heroes";

// Promesas
// Son asicronas, el navegador ejecuta todo lo que es sincrono primero y despues
// ejecutara las partes asincronas.
const promesa = new Promise((resolve, reject) => {    
    // Espera cierta cantidad de tiempo para ejecutar el callback
    setTimeout(() => {
        //console.log("2 segundo despues");

        // Importaremos nuestro array de heroes
        const heroe = getHeroueById(2);
        console.log(heroe);


        // Para indicar que termino con exito llamado a resolve()
        resolve(heroe)

        // Cuando falla llamamos:
        //reject("No se puedo encontrar el heroe")
    }, 2000);
});

// Esto se ejecutara despues de saber si se hizo con exito (termine resolve())
promesa.then((heroe) => {
    //console.log("Then de la promesa");
    console.log("heroe", heroe);
})
.catch( err => console.warn(err));
// Para capturar el error

// Haciendo más general el metodo


const getHeroeByIdAsync = (id) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            const heroe = getHeroueById(id);
            if (heroe) // type coerhion truthy and falsy
                resolve(heroe);
            else 
                reject("No se pudo encontrar el heroe");

        }, 2000);
    });
};

getHeroeByIdAsync(1)
.then(heroe => console.log("Heroe with async", heroe)).catch(err => console.log(err));


getHeroeByIdAsync(3)
.then(heroe => console.log("Heroe with async", heroe))
.catch(err => console.log(err));


getHeroeByIdAsync(10)
.then(heroe => console.log("Heroe with async", heroe))
.catch(err => console.log(err));

// Otra forma simplificada de hacerlo cuando deseamos solo imprimir los mensajes
getHeroeByIdAsync(10)
.then(heroe => console.log("Heroe with async", heroe))
.catch(console.warn);


// Usando esta misma idea, vemos que then y cath son funciones de orden superior
// por lo que solo necesitaremos pasar nuestras funciones para sean ejecutadas
getHeroeByIdAsync(4)
.then(console.log)
.catch(console.warn);


