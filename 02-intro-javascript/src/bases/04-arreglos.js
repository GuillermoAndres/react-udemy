// Arrays en JS
const array_ = new Array();
console.log(array_);

// Con posiciones 5 elementos vacios
const arr = new Array(5);
arr.push(99)
arr.push(46)
console.log(arr);

// Arreglo literales
const arreglo = []  // Creando una referencia constante
// Es un objeto mutable modifica el contendio con push
arreglo.push(1);
arreglo.push(23);
arreglo.push(43);
arreglo.push(109);
console.log(arreglo);

// Copiando referencia
let arre = [1,2,3,4]
let arre2 = arre 
arre2.push(77)
console.log(arre);
console.log(arre2);
console.log('------------');

// Clonando arreglo
arre = [1,2,3,4]
arre2 = [...arre, 77]
console.log(arre);
console.log(arre2);
console.log('------------');
// Arreglo dentro de otro arreglo
arre2 = [arre]
arre2.push(77)
console.log(arre);
console.log(arre2);



// Funcion de orden superior
// Callback una funcion que se va ejecutar adentro de otra funcion
// Esta funcion se ejecuta a cada uno de los elementos
let arre3 = arre.map(function (numero) {
    
});
// Cada funcion en javascript si no tiene un return explicito regresara undefined
console.log(arre3);

// Regresa un numero arreglo con cada valor que regresa la funcion
arre3 = arre.map(function (numero) {
    return 'Hola'
});
console.log(arre3);


arre3 = arre.map(function (numero) {
    return numero * 2
});
console.log(arre3);
