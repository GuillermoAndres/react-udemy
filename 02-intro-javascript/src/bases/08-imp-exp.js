// Importando files

import { heroes } from "../data/heroes";


// 1) Escribilor todo
//import {heroes} from './data/heroes';

// 2) Prefijo imp y saldra un snippte de vscode
// imp

// 3) Agregando la variable importar se agregara el snippet
// heroes

// Para que funcion la importacion debemos de agregar export en la variable
// del archivo a importar
// console.log(heroes);


// const getHeroueById = (id) => {
//     return heroes.find((heroe) => heroe.id === id)
// };

// Simplificando
// find: encuentra el primer elemento que cumpla la condicion
export const getHeroueById = (id) => heroes.find(heroe => heroe.id === id);
//console.log(getHeroueById(2))

// filter: devuelve un array de los elementos que cumple la condicion
export const getHeroesByOwner = (owner) => heroes.filter(heroe => heroe.owner === owner);
// console.log(getHeroesByOwner('DC'));
// console.log(getHeroesByOwner('Marvel'));

// Importacion por defecto
// Otra forma de importar es la importacion por defecto, se coloca en la
// parte de abajo del archivo a importar en este caso heroes.js
// export default heroes
// import heroes from "./data/heroes";

// Importaciones individuales es con {} y solo se utiliza export
// export const heroes = ...
// import { heroes } from "./data/heroes";

// Importacion en solo export
// export {
//     heroes as default,
//     owners,
// }
// import { heroes } from "./data/heroes";
